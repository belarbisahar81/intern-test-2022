FROM python:3.9
ADD main.py .
RUN pip install ArgumentParser
RUN pip install numpy 
RUN pip install tensorflow 
RUN pip install Pillow
CMD [ "python","./main.py" ]