from argparse import ArgumentParser
import glob
import sys
import os
import numpy as np
import tensorflow as tf
from PIL import Image





class_names = ['daisy', 'dandelion', 'roses', 'sunflowers', 'tulips']

def parse_args():
    
    parser = ArgumentParser()
    parser.add_argument("--input_folder", type=str, help="A folder with images to analyze.",default=r"./flowers")
    parser.add_argument("--model_path", type=str, help="path to a tflite model.",default=r"model.tflite") 
    args = parser.parse_args()      
    return args


def max_indice(t):
    x=t[0]
    j=0
    for i in range(len(t)):
        if t[i]>x:
            x=t[i]
            j=i
    return j

if __name__ == "__main__":
    cli_args = parse_args()
    # Load TFLite model 
    tflite_interpreter = tf.lite.Interpreter(model_path=cli_args.model_path)
    tflite_interpreter.allocate_tensors()
    # Get input and output tensors.
    input_details = tflite_interpreter.get_input_details()
    output_details = tflite_interpreter.get_output_details()
    # get shape of input tensors .
    width=input_details[0]['shape'][1]
    hight=input_details[0]['shape'][2]
    # get all images from the input folder .
    for filename in glob.iglob(cli_args.input_folder + '/*.jpg', recursive=True):
        # load the image
        image = Image.open(filename)
        # image resizing based on tensor's input shape (width , hight).
        img_resized = image.resize((width,hight))
        new_img = np.array(img_resized, dtype=np.float32)  
        # Test the model on input data.
        tflite_interpreter.set_tensor(input_details[0]['index'], [new_img])
        # run the inference
        tflite_interpreter.invoke()
        # output_details
        output_data = tflite_interpreter.get_tensor(output_details[0]['index'])
        # apply softmax on the output to get confidence score .
        scores = tf.nn.softmax(output_data)
        # get image name from the image folder.
        img_name=os.path.basename(filename)
        # print the output in the required format on stdout .
        #get only the biggest score and it's corresponding class .
        dic={str(img_name):{"score":scores.numpy()[0][max_indice(scores.numpy()[0])], "class":str(class_names[max_indice(scores.numpy()[0])])}}
        print(dic,file=sys.stdout)   

# 1)The only traces allowed on stdout are results from prediction. 
# 2)all other traces (info, debug, ...) are printed on stderr. 
# to satisfy 1) and 2) : redirect the stderr in another file , exemple : python main.py 2>info.txt 

    sys.stderr.write(f"Analyzing folder : {cli_args.input_folder}")   